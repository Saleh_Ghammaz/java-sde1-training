CREATE DATABASE school;
use school;
SET SQL_SAFE_UPDATES = 0;
CREATE TABLE student(
id int not null,
studentName varchar(20) not null,
average int not null,
email varchar(40) not null,
CONSTRAINT id_PK PRIMARY KEY(id)
);
INSERT INTO student(id,studentName,average,email)VALUES(1,'Ahmad',50,'ahmad@gmail.com');
INSERT INTO student(id,studentName,average,email)VALUES(2,'Ali',70,'ali@gmail.com');
INSERT INTO student(id,studentName,average,email)VALUES(3,'Mohammad',90,'mohammad@gmail.com');
SELECT * FROM student;
UPDATE student 
SET id = 4
WHERE id = 1;
DELETE FROM student 
WHERE id = 4;
SELECT id FROM student WHERE studentName = 'Ali';
SELECT id,studentName FROM student WHERE average > 60;

select * from student


