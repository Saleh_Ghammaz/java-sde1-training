package mysql.project;

public class Model {
	private int id;
	private String studentName;
	private int average;
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getAverage() {
		return average;
	}

	public void setAverage(int average) {
		this.average = average;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(this.id).append(",");
		buf.append(this.studentName).append(",");
		buf.append(this.average).append(",");
		buf.append(this.email).append(",");
		return buf.toString();
	}
}
