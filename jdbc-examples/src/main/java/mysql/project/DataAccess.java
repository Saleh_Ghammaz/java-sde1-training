package mysql.project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.jk.data.dataaccess.JKDataAccessFactory;
import com.jk.data.dataaccess.core.JKDataAccessService;

public class DataAccess {
	JKDataAccessService data = JKDataAccessFactory.getDataAccessService();

	protected Model populateStudent(ResultSet rs) throws SQLException {
		Model m = new Model();
		m.setId((Integer) rs.getObject("id"));
		m.setStudentName((String) rs.getObject("studentName"));
		m.setAverage((Integer) rs.getObject("average"));
		m.setEmail((String) rs.getObject("email"));
		return m;

	}

	public List<Model> getAll() {
		List<Model> listOfStudent = data.getList("SELECT * FROM student", this::populateStudent);
		for (int i = 0; i < listOfStudent.size(); i++) {
			System.out.println(listOfStudent.get(i));
		}
		return listOfStudent;
	}

	public void insertData(Model m) {
		String sql = "INSERT INTO student (id,studentName,average,email) VALUES (?,?,?,?)";
		data.execute(sql, m.getId(), m.getStudentName(), m.getAverage(), m.getEmail());

	}

	public boolean deleteData(int id) {
		return data.execute("DELETE FROM student WHERE id = ? ", id) == 1;
	}

	public boolean updateData(Model m) {
		String sql = "UPDATE student SET studentName=?,average=?,email=? WHERE id=?";
		int records = data.execute(sql, m.getStudentName(), m.getAverage(), m.getEmail(), m.getId());
		return records == 1;
	}

}
