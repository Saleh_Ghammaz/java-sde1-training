package CardGame;

import java.util.ArrayList;

public class Game {

	private String gameName;
	private ArrayList<Player> player;

	public Game(String gameName, ArrayList<Player> player) {
		super();
		this.gameName = gameName;
		this.player = player;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public ArrayList<Player> getPlayer() {
		return player;
	}

	public void setPlayer(ArrayList<Player> player) {
		this.player = player;
	}

}
