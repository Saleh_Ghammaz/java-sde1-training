package CardGame;

import java.util.Arrays;
import java.util.List;

public class Card {

	private String number, cardType;

	public Card(String number, String cardType) {
		setNumber(number);
		setCardType(cardType);
	}
	public Card() {
		
	}
	

	public String getNumber() {
		return number;
	}

	@Override
	public String toString() {
		return "number = " + number + " , cardType = " + cardType + "";
	}

	public void setNumber(String number) {

		List<String> validNumbers = getValidNumbers();
		number = number.toLowerCase();

		if (validNumbers.contains(number))
			this.number = number;
		else
			throw new IllegalArgumentException("Valid numbers are : " + validNumbers);
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {

		List<String> validCardTypes = getValidCardType();
		cardType = cardType.toUpperCase();

		if (validCardTypes.contains(cardType))
			this.cardType = cardType;
		else
			throw new IllegalArgumentException("Valid card types are : " + validCardTypes);

	}

	public static List<String> getValidCardType() {
		return Arrays.asList("HEARTS", "SPADES", "DIAMONDS", "CLUBS");
	}

	public static List<String> getValidNumbers() {
		return Arrays.asList("1","2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king");

	}

}
