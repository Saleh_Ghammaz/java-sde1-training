package CardGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		List<String> numbers = Card.getValidNumbers();
		List<String> cardType = Card.getValidCardType();

		ArrayList<Card> deck = new ArrayList<Card>();

		for (String c : cardType) {
			for (String n : numbers) {
				deck.add(new Card(n, c));
			}

		}

		Collections.shuffle(deck);

		ArrayList<Card> p1 = new ArrayList<Card>();
		ArrayList<Card> p2 = new ArrayList<Card>();
		ArrayList<Card> p3 = new ArrayList<Card>();
		ArrayList<Card> p4 = new ArrayList<Card>();

		for (int i = 0; i <= 12; i++) {
			p1.add(deck.get(i));
		}

		for (int i = 13; i <= 25; i++) {
			p2.add(deck.get(i));
		}

		for (int i = 26; i <= 38; i++) {
			p3.add(deck.get(i));
		}

		for (int i = 39; i <= 51; i++) {
			p4.add(deck.get(i));
		}

		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter the first player's name : ");
		String p1Name = scan.next();
		System.out.print("Please enter the second player's name : ");

		String p2Name = scan.next();
		System.out.print("Please enter the third player's name : ");

		String p3Name = scan.next();
		System.out.print("Please enter the fourth player's name : ");

		String p4Name = scan.next();

		Player player1 = new Player(p1Name, p1);
		Player player2 = new Player(p2Name, p2);
		Player player3 = new Player(p3Name, p3);
		Player player4 = new Player(p3Name, p4);

		ArrayList<Player> cardGame = new ArrayList<Player>();
		cardGame.add(player1);
		cardGame.add(player2);
		cardGame.add(player3);
		cardGame.add(player4);

		System.out.print("Please enter the game name : ");

		String gameName = scan.next();

		Game game = new Game(gameName, cardGame);

		System.out.println("The game name is : " + gameName);
		System.out.println("The player names are : " + p1Name + "// " + p2Name + "// " + p3Name + " //" + p4Name);
		System.out.println("The cards in with first player, including number and type are " + p1);
		System.out.println("The cards in with second player, including number and type are " + p2);
		System.out.println("The cards in with third player, including number and type are " + p3);
		System.out.println("The cards in with fourth player, including number and type are " + p4);

	}

}
