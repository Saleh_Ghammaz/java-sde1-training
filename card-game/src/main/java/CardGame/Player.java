package CardGame;

import java.util.ArrayList;

public class Player {
	
	private String name;
	private int score;
	private ArrayList<Card> card;
	
	
	
	public Player(String name, ArrayList<Card> card) {
		
		this.name = name;
		this.card = card;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public ArrayList<Card> getCard() {
		return card;
	}



	public void setCard(ArrayList<Card> card) {
		this.card = card;
	}

	

}
