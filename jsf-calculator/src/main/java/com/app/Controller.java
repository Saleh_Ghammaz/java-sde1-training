package com.app;

import java.util.ArrayList;
import java.util.Iterator;

import com.jk.web.faces.controllers.JKWebController;

import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;

@Named("controller")
@ViewScoped
public class Controller extends JKWebController {

	String input = "";
	double output = 0;
	private double result = 0;
	char arr[];
	String firstPart = "";
	String lastPart = "";
	String oprater = "";
	Boolean check = true;

	public void startCalc() {
		output = calc();
	}

	public double calc() {
		firstPart = "";
		lastPart = "";
		oprater = "";
		check = true;

		if (!input.isEmpty()) {
			arr = new char[input.length()];

			for (int i = 0; i < arr.length; i++) {
				arr[i] = input.charAt(i);
			}
			for (int i = 0; i < arr.length; i++) {
				if (check) {
					firstPart = firstPart + arr[i];
				}
				if (arr[i] == '+' || arr[i] == '-' || arr[i] == '*' || arr[i] == '/') {
					oprater = oprater + arr[i];
					check = false;
					i++;

				}
				if (!check) {
					lastPart = lastPart + arr[i];
				}
			}
			String firstPartNew = removeLast(firstPart);

			int num1 = Integer.parseInt(firstPartNew);
			int num2 = Integer.parseInt(lastPart);

			switch (oprater) {
			case "+":
				result = num1 + num2;
				break;
			case "-":
				result = num1 - num2;
				break;
			case "/":
				if (num2 == 0) {
					check = !check;

				} else {
					result = num1 / num2;

				}

				break;
			case "*":
				result = num1 * num2;
				break;
			}
		}
		return result;
	}

	public String removeLast(String str) {
		if (str.length() > 0) {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	public String getNum() {
		return input;
	}

	public void addNum(String num) {
		input = input + num;
	}

	public void setResult(double _result) {
		this.result = _result;
	}

	public void clear() {
		input = "";
	}

	public String getInput() {
		return input;
	}

	public double getOutput() {
		return output;
	}

	public void setOutput(double output) {
		this.output = output;
	}

}