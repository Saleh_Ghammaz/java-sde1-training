
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Array {

	public static void main(String[] args) {
		int sum = 0;
		int avg = 0;

		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter a number more than 10000 : ");
		int num = scan.nextInt();
		Random rd = new Random();
		int[] a = new int[num];
		int[] mostFrequentElements = new int[3];
		Map<Integer, Integer> m = new HashMap<Integer, Integer>();

		if (num >= 10000) {

			for (int i = 0; i < a.length; i++)

			{
				a[i] = rd.nextInt(1000) + 1;
				sum += a[i];
				System.out.println(a[i]);
			}
			for (Integer i : a) {
				Integer value = m.get(i);
				m.put(i, (value == null) ? 1 : value + 1);

			}

			int max1 = Integer.MIN_VALUE;
			int element1 = 0;
			for (Map.Entry<Integer, Integer> map : m.entrySet()) {
				if (map.getValue() > max1) {
					max1 = map.getValue();
					element1 = map.getKey();
				}
			}

			System.out.println("Map Size = " + m.size());
			System.out.println(m);

			avg = sum / num;
			System.out.println("Sum of all the array items = " + sum);
			System.out.println("Average of all the array items = " + avg);

			mostFrequentElements[0] = element1;
			m.remove(element1);

			int max2 = Integer.MIN_VALUE;
			int element2 = 0;
			for (Map.Entry<Integer, Integer> map : m.entrySet()) {
				if (map.getValue() > max2) {
					max2 = map.getValue();
					element2 = map.getKey();
				}
			}
			mostFrequentElements[1] = element2;
			m.remove(element2);
			int max3 = Integer.MIN_VALUE;
			int element3 = 0;
			for (Map.Entry<Integer, Integer> map : m.entrySet()) {
				if (map.getValue() > max3) {
					max3 = map.getValue();
					element3 = map.getKey();
				}
			}
			mostFrequentElements[2] = element3;

			System.out.print("Most frequent three numbers = ");

			for (int i = 0; i < mostFrequentElements.length; i++) {
				System.out.print(" " + mostFrequentElements[i]);
			}

		} else
			System.out.println("invalid entry");

	}

}
